# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import requests
import jsonpickle

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionQuestCompleted(Action):
    def name(self) -> Text:
        return "action_quest_completed"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        answer = tracker.get_slot('answer')
        r = requests.get('http://localhost:5000/getNextQuest/{}/{}'.format(tracker.sender_id, answer))

        next_quest = jsonpickle.decode(r.content)

        if next_quest == False:
            text = "Risposta sbagliata, tenta ancora o chiedimi un suggerimento se sei in difficoltà"
        elif next_quest == -1:
            text = "Hai risolto tutti gli enigmi, i risultati verranno annunciati al termine della visita"
        else:
            text = "Perfetto, USER_NAME, risposta corretta, il prossimo enigma è:\n{}".format(next_quest)

        dispatcher.utter_message(text=text)
        return []

class ActionTipReply(Action):
    def name(self) -> Text:
        return "action_tip_reply"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        r = requests.get('http://localhost:5000/getSingleUserTip/{}'.format(tracker.sender_id))
        tip = jsonpickle.decode(r.content)

        if tip == -2:
            text = "Hai risolto tutti gli enigmi, i risultati verranno annunciati al termine della visita"
        elif tip == -1:
            text = "Sono terminati i suggerimenti per questo enigma"
        else:
            text = "Ti do un piccolo suggerimento... {}".format(tip)

        dispatcher.utter_message(text=text)
        return []
