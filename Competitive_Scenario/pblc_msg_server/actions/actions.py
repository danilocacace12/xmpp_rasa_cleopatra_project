# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import requests
import jsonpickle

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionNewUser(Action):
    ISTRUCTION = "Il gioco inizia!\nVi invierò qui la prima missione da completare,"\
    " dovrete inviarmi la soluzione in privato, per ottenere la missione successiva.\n"\
    "Ognuno ha inoltre a disposizione un suggerimento da chiedere in privato, dopo di che"\
    " sarà possibile chiederne un altro solo quando anche gli altri giocatori l'avranno chiesto"\
    "\n\nChi nella durata della visita completerà il nummero maggiori di missioni avrà vinto\n\n"\
    "In qualunque momento potete chiedermi curiosità riguardanti il museo, per avere più informazioni"\
    " che vi aiuteranno nella ricerca"

    def name(self) -> Text:
        return "action_new_user"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user_name = tracker.get_slot('new_user_name')

        if  jsonpickle.decode(requests.get('http://localhost:5000/userPresence/{}'.format(user_name)).content) == None:
                # Aggiungo l'utente alla lista
                requests.post('http://localhost:5000/addNewUser/{}'.format(user_name))
                # Invio messaggio di benvenuto
                welcome = "Benvenuto {}, sono Cleopatra, appena saranno entrati tutti i giocatori inizieremo".format(user_name)
                dispatcher.utter_message(text=welcome)

                r = requests.get('http://localhost:5000/getPlayersNumber')

                if(jsonpickle.decode(r.content) == 2):
                    istruction  = self.ISTRUCTION
                    dispatcher.utter_message(text=self.ISTRUCTION)

                    r = requests.get('http://localhost:5000/startGame')
                    first_quest = jsonpickle.decode(r.content)
                    dispatcher.utter_message(text=first_quest)
        return []

class ActionUnactiveUsers(Action):
    def name(self) -> Text:
        return "action_unactive_users"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        r = requests.get('http://localhost:5000/getRandomCuriosity')
        curiosity = "Lo sapevate che {}?".format(title)
        dispatcher.utter_message(text=curiosity)
        return []

class ActionPeriodicCheck(Action):
    def name(self) -> Text:
        return "action_periodic_check"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        r = requests.get('http://localhost:5000/getUserLessHelped')
        name = jsonpickle.decode(r.content)
        text = "{} tutti i tuoi avversari hanno chiesto un suggerimento\n"
                "Scrivimi in privato per ottenerlo".format(name)
        dispatcher.utter_message(text=text)

        r = requests.get('http://localhost:5000/getHelpLateUser')
        if jsonpickle.decode(r.content) != False:
            [task_index, tip] = jsonpickle.decode(r.content)
            text = "Un piccolo aiuto per completare l'enigma {}: \n{}".format(task_index, tip)
            dispatcher.utter_message(text=text)
        return []
