#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import slixmpp
import threading
import time
import datetime
import requests
import jsonpickle
import json
import string

from abc import ABC, abstractmethod
from muc_abstract import AbstractMUC

class MUC(AbstractMUC):
    def __init__(self):
        super().__init__('http://localhost:5015/webhooks/rest/webhook',
                         'http://localhost:5016/webhooks/rest/webhook',
                         'http://localhost:5014/webhooks/rest/webhook',
                         'bot@localhost',
                         'bot',
                         'museum@conference.localhost',
                         'cleopatra',
                         20,
                         True)

if __name__ == '__main__':
    xmpp = MUC()
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0045') # Multi-User Chat
    xmpp.register_plugin('xep_0199') # XMPP Ping
    # Connect to the XMPP server and start processing XMPP stanzas.
    xmpp.connect()
    xmpp.process()
