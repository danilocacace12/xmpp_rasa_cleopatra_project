# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import requests
import jsonpickle

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionGetBooks(Action):
    def name(self) -> Text:
        return "action_get_books"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        category = tracker.get_slot('category')
        r = requests.get('http://localhost:5000/getBooksFromCategory/{}'.format(category))
        books_list = jsonpickle.decode(r.content)
        answer = "USER_NAME, ti potrebbero piacere anche:\n"
        for book in books_list:
            answer += "- {}\n".format(book)
        answer += """Questi libri sono tutti disponibili,
                    taggami nel messaggio se vuoi che organizzi una spedizione per te"""
        dispatcher.utter_message(text=answer)
        return []

class ActionUnactiveUsers(Action):
    def name(self) -> Text:
        return "action_unactive_users"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        r = requests.get('http://localhost:5000/getRandomTitle')
        title = jsonpickle.decode(r.content)
        text = "Cosa ne pensate di {}?".format(title)
        dispatcher.utter_message(text=text)
        return []

class ActionNewUser(Action):
    def name(self) -> Text:
        return "action_new_user"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user_name = tracker.get_slot('new_user_name')

        if  jsonpickle.decode(requests.get('http://localhost:5000/getUserByName/{}'.format(
                                                                    user_name)).content) == None:
            # Aggiungo l'utente alla lista
            requests.post('http://localhost:5000/addNewUser/{}'.format(user_name))
            # Invio messaggio di benvenuto
            text = "Benvenuto {}, sono il Bibliotecario, sii sempre cordiale,"
                    "questo gruppo è nato per lo scambio gratuito di libri".format(user_name)

            dispatcher.utter_message(text=text)
        return []
