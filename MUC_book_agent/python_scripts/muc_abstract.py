#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import slixmpp
import threading
import time
import datetime
import requests
import jsonpickle
import json
import string

from abc import ABC, abstractmethod

class AbstractMUC(ABC, slixmpp.ClientXMPP):
    API_PRIVATE_MSG = None
    API_TAGGED_MSG = None
    API_PUBBLIC_MSG = None
    time_to_unactive = None
    time_stamp_old = datetime.datetime.now()
    time_stamp_new = datetime.datetime.now()

    @abstractmethod
    def __init__(self,
                    API_PRIVATE_MSG,
                    API_TAGGED_MSG,
                    API_PUBBLIC_MSG,
                    jid,
                    password,
                    room,
                    nick,
                    seconds_to_unactive):
        self.API_PRIVATE_MSG = API_PRIVATE_MSG
        self.API_TAGGED_MSG = API_TAGGED_MSG
        self.API_PUBBLIC_MSG  = API_PUBBLIC_MSG
        self.seconds_to_unactive = seconds_to_unactive

        slixmpp.ClientXMPP.__init__(self, jid, password)
        self.room = room
        self.nick = nick

        self.add_event_handler("session_start", self.start)
        self.add_event_handler("groupchat_message", self.muc_message)
        self.add_event_handler("message", self.private_message)
        self.add_event_handler("muc::{}::presence".format(self.room),
                           self.new_user)

        thread = threading.Thread(target = self.unactive_users)
        thread.daemon = True
        thread.start()

    def start(self, event):
        self.get_roster()
        self.send_presence()
        self.plugin['xep_0045'].join_muc(self.room,
                                         self.nick,
                                         wait=True)


    def new_user(self, presence):
        if presence['muc']['nick'] != self.nick:
            payload ={
                    "sender":"new_user",
                    "message":"NEW_USER {}".format(presence['muc']['nick'])
                }
            r = requests.post(self.API_PUBBLIC_MSG, data=json.dumps(payload))
            text = r.json()[0]['text'].replace('USER_NAME', msg['mucnick'])
            #Ritorno la risposta nella chat
            self.send_message(mto=self.room,
                          mbody=text,
                          mtype='groupchat')


    def muc_message(self, msg):
        print("papera")
        if msg['mucnick'] != self.nick:
            self.time_stamp_old = self.time_stamp_new
            self.time_stamp_new = datetime.datetime.now()
            if self.nick in msg['body']:
                self.muc_tagged_message(msg)
            else:
                self.muc_untagged_message(msg)

    def muc_untagged_message(self, msg):
        print("papera")
        payload ={
                    "sender":msg['mucnick'],
                    "message":msg['body']
                }
        r = requests.post(self.API_PUBBLIC_MSG, data=json.dumps(payload))
        text = r.json()[0]['text'].replace('USER_NAME', msg['mucnick'])
        #Ritorno la risposta nella chat
        self.send_message(mto=self.room,
                          mbody=text,
                          mtype='groupchat')


    def muc_tagged_message(self, msg):
        message = msg['body'].replace(self.nick, '')
        payload ={
                    "sender":msg['mucnick'],
                    "message":message
                }
        r = requests.post(self.API_TAGGED_MSG, data=json.dumps(payload))
        text = r.json()[0]['text'].replace('USER_NAME', msg['mucnick'])
    #Ritorno la risposta nella chat
        self.send_message(mto=self.room,
                          mbody=text,
                          mtype='groupchat')

    def private_message(self, msg):
        if msg['mucnick'] != self.nick and msg['type'] == 'chat':
            nickname = str(msg['from']).replace("{}/".format(self.room), '')
            payload ={
                        "sender":nickname,
                        "message":msg['body']
                    }
            r = requests.post(self.API_PRIVATE_MSG, data=json.dumps(payload))
            print(r.json())
            text = r.json()[0]['text'].replace('USER_NAME', nickname)
            self.send_message(mto=msg['from'],
                            mbody=text,
                            mtype='chat')

    def unactive_users(self):
        starttime = time.time()
        while True:
            delta = datetime.datetime.now() - self.time_stamp_new
            if delta.seconds > self.seconds_to_unactive:
                payload ={
                            "sender":"bridge",
                            "message":"UNACTIVE_USERS"
                        }
                r = requests.post(self.API_PUBBLIC_MSG, data=json.dumps(payload))
                text = r.json()[0]['text']
            #Ritorno la risposta nella chat
                self.send_message(mto=self.room,
                                  mbody=text,
                                  mtype='groupchat')
            time.sleep(10.0 - ((time.time() - starttime) % 10.0))
