#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time
import datetime
import logging
import slixmpp
import string
import requests
import jsonpickle
import json

from threading import Thread
from muc_abstract import AbstractMUC
from getpass import getpass
from argparse import ArgumentParser
from shared_storage import User

class TimeChecker(Thread):
    SLEEP_TIME = 60

    def __init__(self, delay, muc):
        Thread.__init__(self)
        self.muc = muc
        self.delay = delay
        self.time_stamp_old = datetime.datetime.now()
        self.time_stamp_new = datetime.datetime.now()

    def updateTimestamp(self, time_stamp):
        self.time_stamp_old = self.time_stamp_new
        self.time_stamp_new = time_stamp

    def run(self):
        while True:
            delta = datetime.datetime.now() - self.time_stamp_new
            if delta.seconds > self.delay:
                self.muc.unactive_users()
            time.sleep(self.SLEEP_TIME)

class MUC(AbstractMUC, slixmpp.ClientXMPP):
    API_PRIVATE_MSG = 'http://localhost:5015/webhooks/rest/webhook'
    API_TAGGED_MSG = 'http://localhost:5016/webhooks/rest/webhook'
    API_PUBBLIC_MSG = 'http://localhost:5014/webhooks/rest/webhook'

    def __init__(self, jid, password, room, nick):
        self.time_checker = TimeChecker(60, self)
        self.time_checker.start()

        slixmpp.ClientXMPP.__init__(self, jid, password)
        self.room = room
        self.nick = nick

        self.add_event_handler("session_start", self.start)
        self.add_event_handler("groupchat_message", self.muc_message)
        self.add_event_handler("message", self.private_message)
        self.add_event_handler("muc::{}::presence".format(self.room),
                           self.new_user)

    def start(self, event):
        self.get_roster()
        self.send_presence()
        self.plugin['xep_0045'].join_muc(self.room,
                                         self.nick,
                                         wait=True)

    def new_user(self, presence):
        # Se l'utente non è già presente (è andato momentaneamente offline)
        if presence['muc']['nick'] != self.nick and jsonpickle.decode(
                                                                requests.get('http://localhost:5000/getUserByName/{}'.format(presence['muc']['nick']))
                                                                .content) == None:
            # Aggiungo l'utente alla lista
            requests.post('http://localhost:5000/addNewUser/{}'.format(presence['muc']['nick']))
            # Invio messaggio di benvenuto
            print(presence['from'].bare)
            self.send_message(mto=presence['from'].bare,
                              mbody="Benvenuto {}, sono il Bibliotecario, sii sempre cordiale,"
                              "questo gruppo è nato per lo scambio gratuito di libri".format(presence['muc']['nick']),
                              mtype='groupchat')


    def muc_message(self, msg):
        if msg['mucnick'] != self.nick:
            self.time_checker.updateTimestamp(datetime.datetime.now())
            if self.nick in msg['body']:
                self.muc_tagged_message(msg)
            else:
                self.muc_untagged_message(msg)

    def muc_untagged_message(self, msg):
        payload ={
                    "sender":msg['mucnick'],
                    "message":msg['body']
                }
        r = requests.post(self.API_PUBBLIC_MSG, data=json.dumps(payload))
        text = r.json()[0]['text'].replace('USER_NAME', msg['mucnick'])
        #Ritorno la risposta nella chat
        self.send_message(mto=self.room,
                          mbody=text,
                          mtype='groupchat')

    def muc_tagged_message(self, msg):
        message = msg['body'].replace(self.nick, '')
        payload ={
                    "sender":msg['mucnick'],
                    "message":message
                }
        r = requests.post(self.API_TAGGED_MSG, data=json.dumps(payload))
        text = r.json()[0]['text'].replace('USER_NAME', msg['mucnick'])
        #Ritorno la risposta nella chat
        self.send_message(mto=self.room,
                          mbody=text,
                          mtype='groupchat')

    def private_message(self, msg):
        if msg['mucnick'] != self.nick and msg['type'] == 'chat':
            nickname = str(msg['from']).replace("{}/".format(self.room), '')
            payload ={
                        "sender":nickname,
                        "message":msg['body']
                    }
            r = requests.post(self.API_PRIVATE_MSG, data=json.dumps(payload))
            print(r.json())
            text = r.json()[0]['text'].replace('USER_NAME', nickname)
            self.send_message(mto=msg['from'],
                            mbody=text,
                            mtype='chat')

    def unactive_users(self):
        # L'Agente cerca di mantenere viva la conversazione
        r = requests.get('http://localhost:5000/getRandomTitle')
        title = jsonpickle.decode(r.content)
        text = "Cosa ne pensate di {}?".format(title)
        self.send_message(mto=self.room, mbody=text,mtype='groupchat')



if __name__ == '__main__':
    # Setup the command line arguments.
    parser = ArgumentParser()

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    parser.add_argument("-r", "--room", dest="room",
                        help="MUC room to join")
    parser.add_argument("-n", "--nick", dest="nick",
                        help="MUC nickname")

    args = parser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    if args.jid is None:
        args.jid = input("Username: ")
    if args.password is None:
        args.password = getpass("Password: ")
    if args.room is None:
        args.room = input("MUC room: ")
    if args.nick is None:
        args.nick = input("MUC nickname: ")

    # Setup the MUCBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = MUC(args.jid, args.password, args.room, args.nick)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0045') # Multi-User Chat
    xmpp.register_plugin('xep_0199') # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    xmpp.connect()
    xmpp.process()
