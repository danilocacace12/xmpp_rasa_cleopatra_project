#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions

from user_management import *
import requests
import jsonpickle
# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
#
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionSetStory(Action):
    def name(self) -> Text:
        return "action_set_story"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        print(tracker.sender_id)
        print(tracker.latest_message['text'])
        dispatcher.utter_message(text="Hello World!")
        return []

# TODO: Sarebbe possibile fare una sola azione che verifica lo step corrente dall'user_management e setta lo slot corretto chiamandolo
#        sender_id+slot_number
# TODO: La risposta dovrebbe dipendere anche 
class Check0000001step1(Action):
    def name(self) -> Text:
        return "check_0000001_step1"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        data = jsonpickle.encode(True)
        requests.post('http://localhost:5000/stepSetter/{}/0'.format(tracker.sender_id), data = data)
        dependency_status = jsonpickle.decode(requests.get('http://localhost:5000/getDependencyValue/{}/0'.format(tracker.sender_id)).content)
        #print(tracker.latest_message['text'])
        #dispatcher.utter_message(text="Hello World!")
        return [SlotSet("000001_step1", dependency_status)]

class Check0000010step1(Action):
    def name(self) -> Text:
        return "check_0000010_step1"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        data = jsonpickle.encode(True)
        requests.post('http://localhost:5000/stepSetter/{}/0'.format(tracker.sender_id), data = data)
        dependency_status = jsonpickle.decode(requests.get('http://localhost:5000/getDependencyValue/{}/0'.format(tracker.sender_id)).content)
        return [SlotSet("000010_step1", dependency_status)]
