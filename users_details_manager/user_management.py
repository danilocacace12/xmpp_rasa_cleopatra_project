#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from task_management import *

from flask import Flask, jsonify, request, abort, make_response
import requests
import jsonpickle


# La classe user mi rappresenta l'utente con tutte le sue caratteristiche
class User:
    def __init__(self, name):
        self.name = name
        self.ready = False
        self.task = None

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setReady(self, ready):
        self.ready = ready

    def getReady(self):
        return self.ready

    def setTask(self, task):
        self.task = task

    def getTask(self):
        return self.task


class Singleton (type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


# La classe UserManager permette di gestire tutti gli utenti e le caratteristiche che hanno
class UserManager(metaclass=Singleton):
    users_list = None
    t_manager = None

    def __init__(cls, *args):
        if len(args) == 1:
            users_list = args[0]
            if cls.users_list is None and cls.t_manager is None:
                cls.users_list = users_list
                cls.t_manager = TaskManager()
        elif len(args) ==0:
            cls.users_list = []
            cls.t_manager = TaskManager()

    def getTaskManager(cls):
        return cls.t_manager

    def setAllUser(cls, users_list):
        cls.users_list = users_list

    def removeAllUsers(cls):
        cls.users_list = []

    def addNewUser(self, user):
        self.users_list.append(user)
        return user

    def removeUser(self, name):
        user = self.getUserByName(name)
        self.users_list.remove(user)

    def getUserByName(self, name):
        for user in self.users_list:
            if user.getName() == name:
                return user
        return None

    def getUserListNames(self):
        names = []
        for user in self.users_list:
            names.append(user.getName())
        return names

    def getAllUsers(self):
        return self.users_list

    def change_name(self, user, new_name):
        index = self.users_list.index(user)
        self.users_list[index].setName(new_name)

    def userReady(self, user):
        index = self.users_list.index(user)
        self.users_list[index].setReady(True)

    # Ritorno true se tutti gli utenti sono pronti a giocare
    def checkAllReady(self):
        check = True
        for user in self.users_list:
            if user.getReady() == False:
                check = False
        return check

    def checkReady(self, name):
        return self.getUserByName(name).getReady()

    # Risotrno la lista di utenti che non sono ancora pronti a giocare
    def getUnreadyUsersList(self):
        users_not_ready = []
        for user in self.users_list:
            if user.getReady() == False:
                users_not_ready.append(user)
        return users_not_ready

    # Ritorno il valore attualte della variabile corrispondente allo step in cui mi trovo nella storia
    def stepChecker(self, user, step_number):
        task = user.getTask()
        return task.getSteps()[step_number]

    # Cambio il valore in una certa posizione del vettore steps, quando avviene qualcosa nella storia
    def stepSetter(self, user, step_number, value):
        task = user.getTask()
        steps = task.getSteps()
        steps[step_number] = value
        task.setSteps(steps)

    # Ritorna il valore dello step attuale del task di cui è dipendente il task dell'utente in esame
    def getDependecyValue(self, user, step_number):
        task = user.getTask()
        id_task = task.getDependencies()[step_number]
        for user in self.users_list:
            if user.getTask().getIdTask() == id_task:
                return user.getTask().getSteps()[step_number]


class UserManagerAPI:
    app = Flask(__name__)

    def __init__(cls):
        cls.app.run()

    @app.errorhandler(404)
    def not_found(error):
        return make_response(jsonify({'error': 'Not found'}), 404)

    @app.route('/getUserListNames', methods=['GET'])
    def getUserListNamesAPI():
        return jsonpickle.encode(UserManager().getUserListNames())

    @app.route('/addNewUser', methods=['POST'])
    def addNewUserAPI():
        if not request.data:
            abort(404)
        user = jsonpickle.decode(request.data)
        return jsonpickle.encode(UserManager().addNewUser(user))

    @app.route('/checkAllReady', methods=['GET'])
    def checkAllReadyAPI():
        return jsonpickle.encode(UserManager().checkAllReady())

    @app.route('/checkReady/<name>', methods=['GET'])
    def checkReadyAPI(name):
        return jsonpickle.encode(UserManager().checkReady(name))

    @app.route('/userReady/<name>', methods=['GET'])
    def userReadyAPI(name):
        user = UserManager().getUserByName(name)
        UserManager().userReady(user)
        return jsonpickle.encode({'success':True}), 200

    @app.route('/getUnreadyUsersList')
    def getUnreadyUsersListAPI():
        return jsonpickle.encode(UserManager().getUnreadyUsersList())

    @app.route('/getUserByName/<name>', methods=['GET'])
    def getUserByNameAPI(name):
        return jsonpickle.encode(UserManager().getUserByName(name))

    @app.route('/removeUser/<name>', methods=['DELETE'])
    def removeUserAPI(name):
        UserManager().removeUser(name)
        return  jsonpickle.encode({'success':True}), 200

    @app.route('/getAllUsers', methods = ['GET'])
    def getAllUsersAPI():
        return jsonpickle.encode(UserManager().getAllUsers())

    @app.route('/stepChecker/<name>/<int:step_number>', methods=['GET'])
    def stepCheckerAPI(name, step_number):
        user = UserManager().getUserByName(name)
        return jsonpickle.encode(UserManager().stepChecker(user, step_number))

    @app.route('/stepSetter/<name>/<int:step_number>', methods=['POST'])
    def stepSetterAPI(name, step_number):
        if not request.data:
            abort(404)
        user = UserManager().getUserByName(name)
        value = jsonpickle.decode(request.data)
        UserManager().stepSetter(user, step_number, value)
        return jsonpickle.encode({'success':True}), 200

    @app.route('/getDependencyValue/<name>/<int:step_number>', methods=['GET'])
    def getDependecyValueAPI(name, step_number):
        user = UserManager().getUserByName(name)
        return jsonpickle.encode(UserManager().getDependecyValue(user, step_number))

    # FIXME: Da modificare in base al metodo di assegnazione task che verrà stabilito
    @app.route('/TaskManager/assignTask', methods=['GET'])
    def assignTaskAPI():
        t_manager = UserManager().getTaskManager()
        users_list = t_manager.assign(2, 1, UserManager().getAllUsers(), [])
        UserManager().setAllUser(users_list)
        return jsonpickle.encode({'success':True}), 200


def main():
    api = UserManagerAPI()

if __name__ == '__main__':
    main()
