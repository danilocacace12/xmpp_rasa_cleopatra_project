#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random

class GenericTask:
    def __init__(self, id_task, description, is_traitor, steps, dependencies):
        self.id_task = id_task
        self.description = description
        self.is_traitor = is_traitor
        self.steps = steps
        self.dependencies = dependencies

    def setDescription(self, description):
        self.description = description

    def getDescription(self):
        return self.description

    def setIdTask(self, id_task):
        self.id_task = id_task

    def getIdTask(self):
        return self.id_task

    def setIsTraitor(self, is_traitor):
        self.is_traitor = is_traitor

    def getIsTraitor(self):
        return self.is_traitor

    def setSteps(self, steps):
        self.steps = steps

    def getSteps(self):
        return self.steps

    def setDependecies(self, dependencies):
        self.dependencies = dependencies

    def getDependencies(self):
        return self.dependencies


class MercenarioTask(GenericTask):
    def __init__(self):
        super().__init__('s000001',
                        "Il giocatore dovrà trovare un manufatto e riportare un'iscrizione su di essa. "\
                        "Nel caso il traditore completi la sua missione allora sarà necessario raggiungere"\
                        "un ulteriore manufatto, evitando di essere ucciso colpendo chi ritiene essere il traditore",
                        False,
                        [False],
                        ['s000010'])


class TraitorTask(GenericTask):
    def __init__(self):
        super().__init__('s000002',
                        "Il giocatore potrà trovando un'anfora (utilizzata per riti alle divinità)"\
                        "modificare una parola chiave dell'iscrizione necessaria a 000001. "\
                        "Successivamente potrà uccidere il giocatore 000001 cercando di rimanere anonimo",
                        True,
                        [False],
                        [])


class MercanteTask(GenericTask):
    def __init__(self):
        super().__init__('s000010',
                        "Il giocatore rappresenta la figura del mercante che dovrà trovare il suo carro e consegnare l'oro\
                        \ al mercenario, difendendosi dal traditore che può derubarlo",
                        False,
                        [False],
                        ['s000001'])


class TaskManager:
    # TODO: Creare un bacino di Task dal quale attingere durante l'assegnazione
    task_mercenario = MercenarioTask()
    task_traditore = TraitorTask()
    task_mercante = MercanteTask()

    def assign(cls, players_number, traitors_number, users_list, tasks_list):
        # TODO: In questa funzione devo andare a selezionare in modo randomico
        #       gli utenti che sono traditori ed assegnare i task agli utenti
        tasks_list2 = [cls.task_mercenario,cls.task_mercante]
        for i in range(0, len(users_list)):
            users_list[i].setTask(tasks_list2[i])
        return users_list
