#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from getpass import getpass
from argparse import ArgumentParser
import requests
import jsonpickle
import json

from user_management import User
from task_management import GenericTask
import threading
import slixmpp
import string
import time
import datetime
from threading import Thread

class TimeChecker(Thread):
    def __init__(self, delay, time_stamp):
        Thread.__init__(self)
        self.delay = delay
        self.time_stamp_old = time_stamp
        self.time_stamp_new = time_stamp

    def updateTimestamp(self, time_stamp):
        self.time_stamp_old = self.time_stamp_new
        self.time_stamp_new = time_stamp

    def run(self):
        while True:
            delta = self.time_stamp_new - self.time_stamp_old
            if delta.seconds > self.delay:
                # TODO: Da inserire chiamata all'azione da eseguire quando gli utenti non sono attivi
                print('Eseguo azione')
            print(self.time_stamp)
            time.sleep(10)


class MUCBot(slixmpp.ClientXMPP):

    def __init__(self, jid, password, room, nick):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        self.room = room
        self.nick = nick
        self.lm_timestamp = datetime.datetime.now()
        self.time_checker = TimeChecker(10, self.lm_timestamp)
        self.time_checker.start()

        self.add_event_handler("session_start", self.start)
        self.add_event_handler("groupchat_message", self.muc_message)
        self.add_event_handler("muc::{}::presence".format(self.room),
                           self.muc_online)

    def start(self, event):
        self.get_roster()
        self.send_presence()
        self.plugin['xep_0045'].join_muc(self.room,
                                         self.nick,
                                         wait=True)

    def muc_message(self, msg):
        # Verifico che il messaggio non sia quello inviato dal BOT stesso
        if msg['mucnick'] != self.nick:
            self.time_checker.updateTimestamp(datetime.datetime.now())
            # Questa porzione di codice viene eseguita solo ad inizio sessione di gioco, quando gli utenti saranno tutti
            # pronti e quindi i partita non verrà eseguita
            all_ready = jsonpickle.decode(requests.get('http://localhost:5000/checkAllReady').content)
            if not all_ready:
                ready = jsonpickle.decode(
                            requests.get('http://localhost:5000/checkReady/{}'.format(msg['mucnick'])).content)
                if ready == False:
                    msg['body'].translate(str.maketrans('', '', string.punctuation))
                    if msg['body'] == "si" or msg['body'] == "SI" or msg['body'] == "Si":
                        requests.get('http://localhost:5000/userReady/{}'.format(msg['mucnick']))
                    if msg['body'] == "no" or msg['body'] == "NO" or msg['body'] == "No":
                        text = '{} invia "SI" quando sei pronto'.format(msg['mucnick'])
                        self.send_message(mto=self.room, mbody=text, mtype='groupchat')
                    all_ready = jsonpickle.decode(requests.get('http://localhost:5000/checkAllReady').content)
                    if all_ready:
                        self.send_message(mto=self.room,
                                          mbody="Il gioco inizia!\n Ogni partecipante ha ricevuto una missione da completare in privato "\
                                          "attenti...tra di voi c'è un traditore!",
                                          mtype='groupchat')
                        # Assegnamo i task agli utenti chiamando un metodo della classe task_u_manager, questo ci restituisce un dizionario
                        # {utente:task}

                        # FIXME: Interfaccia del metodo assign da modificare
                        requests.get('http://localhost:5000/TaskManager/assignTask')
                        users_list = jsonpickle.decode(requests.get('http://localhost:5000/getAllUsers').content)
                        for user in users_list:
                            print(user)
                            print(user.getTask())
                            payload ={
                                        "sender":user.getName(),
                                        "message": user.getTask().getIdTask()
                            }

                            r = requests.post('http://localhost:5005/webhooks/rest/webhook',
                                               data=json.dumps(payload))
                            text = r.json()[0]['text'].replace('USER_NAME', user.getName())
                            self.send_message(mto='{}/{}'.format(self.room,user.getName()),
                                              mbody=text,
                                              mtype='chat')
                    return

                # TODO: Implementare l'inoltro dei messaggi dalla chat di gruppo al server RASA e girarne le risposte
                #       bisogna implementare anche dei criteri secondi i quali rispondere o meno ai messaggi
                #       che possono essere:
                #       - Richieste di aiuto (posizione o suggerimenti)
                #       - Richieste informazioni su avversari
                #       - Discussione su chi è l'impostore
            payload ={
                        "sender":msg['mucnick'],
                        "message":msg['body']
                    }

            r = requests.post('http://localhost:5005/webhooks/rest/webhook',
                               data=json.dumps(payload))
            text = r.json()[0]['text'].replace('USER_NAME', msg['mucnick'])
            #Ritorno la risposta nella chat
            self.send_message(mto=self.room,
                              mbody=text,
                              mtype='groupchat')


    def muc_online(self, presence):
        # Se l'utente non è già presente (è andato momentaneamente offline)
        if presence['muc']['nick'] != self.nick and jsonpickle.decode(requests.get('http://localhost:5000/getUserByName/{}'.format(presence['muc']['nick'])).content) == None:
            # Aggiungo l'utente alla lista e inoltro una richiesta per iniziare
            data = jsonpickle.encode(User(presence['muc']['nick']))
            requests.post('http://localhost:5000/addNewUser', data = data)
            self.send_message(mto=presence['from'].bare,
                              mbody="Benvenuto {}, sono Bottum, sei pronto a giocare? (si/no)".format(presence['muc']['nick']),


if __name__ == '__main__':
######## DA CANCELLARE PER AVVIARLO DA SHELL CON LOG ECC
    #xmpp = MUCBot('test2@localhost', 'test2', 'stanza@conference.localhost', 'BOT')
    #xmpp.register_plugin('xep_0030') # Service Discovery
    #xmpp.register_plugin('xep_0045') # Multi-User Chat
    #xmpp.register_plugin('xep_0199') # XMPP Ping
    #xmpp.connect()
    #xmpp.process()
#########

    # Setup the command line arguments.
    parser = ArgumentParser()

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    parser.add_argument("-r", "--room", dest="room",
                        help="MUC room to join")
    parser.add_argument("-n", "--nick", dest="nick",
                        help="MUC nickname")

    args = parser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    if args.jid is None:
        args.jid = input("Username: ")
    if args.password is None:
        args.password = getpass("Password: ")
    if args.room is None:
        args.room = input("MUC room: ")
    if args.nick is None:
        args.nick = input("MUC nickname: ")

    # Setup the MUCBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = MUCBot(args.jid, args.password, args.room, args.nick)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0045') # Multi-User Chat
    xmpp.register_plugin('xep_0199') # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    xmpp.connect()
    xmpp.process()
