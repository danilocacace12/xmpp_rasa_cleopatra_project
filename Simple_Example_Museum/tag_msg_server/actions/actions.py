# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import requests
import jsonpickle

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionGetAvailability(Action):
    def name(self) -> Text:
        return "action_get_availability"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        book_name = tracker.get_slot('book_name')
        r = requests.get('http://localhost:5000/checkBookAvailability/{}'.format(book_name))
        availability = jsonpickle.decode(r.content)
        if availability == False:
            answer = "USER_NAME, unfortunately this product is not available."
        else:
            r = requests.post('http://localhost:5000/setUserInterest/{}/{}'.format(tracker.sender_id, book_name))
            answer = "USER_NAME, perfect! This product is available, please contact me in private for organizing the shipment"
        dispatcher.utter_message(text=answer)
        return []
