# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import requests
import jsonpickle

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionGetBooks(Action):
    def name(self) -> Text:
        return "action_get_books"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        category = tracker.get_slot('category')
        r = requests.get('http://localhost:5000/getBooksFromCategory/{}'.format(category))
        books_list = jsonpickle.decode(r.content)
        answer = "USER_NAME, ti potrebbero piacere anche:\n"
        for book in books_list:
            answer += "- {}\n".format(book)
        answer += "Questi libri sono tutti disponibili, taggami nel messaggio se vuoi che organizzi una spedizione per te"
        dispatcher.utter_message(text=answer)
        return []
