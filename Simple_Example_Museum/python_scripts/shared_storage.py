#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import requests
import jsonpickle
import random

from flask import Flask, jsonify, request, abort, make_response



class Singleton (type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

# La classe user mi rappresenta l'utente con tutte le sue caratteristiche
class Book:
    def __init__(self, title, category):
        self.title = title
        self.category = category

    def setTitle(self, title):
        self.title = title

    def getTitle(self):
        return self.title

    def setCategory(self, category):
        self.category = category

    def getCategory(self):
        return self.category

class User:
    def __init__(self, name):
        self.name = name
        self.interest = None

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setInterest(self, interest):
        self.interest = interest

    def getInterest(self):
        return self.interest

# La classe UserManager permette di gestire tutti gli utenti e le caratteristiche che hanno
class Library(metaclass=Singleton):
    book1 = Book("keychain", "fantascienza")
    book2 = Book("pen", "fantascienza")
    book_list = [book1, book2]

    def __init__(self):
        return

    def checkBook(self,book_title):
        for book in self.book_list:
            if book.getTitle() == book_title:
                return True
        return False

    def getBooksFromCategory(self, category):
        books = []
        for book in self.book_list:
            if book.getCategory() == category:
                books.append(book.getTitle())
        return books

    def getRandomTitle(self):
        rnd = random.randint(0, len(self.book_list)-1)
        return self.book_list[rnd].getTitle()

class UserManager(metaclass=Singleton):
    user_list = None

    def __init__(cls, *args):
        if len(args) == 1:
            user_list = args[0]
            if cls.user_list is None:
                cls.user_list = users_list
        elif len(args) ==0:
            cls.user_list = []

    def addNewUser(self, name):
        user = User(name)
        print(user.getName())
        self.user_list.append(user)

    def getUserInterest(self, name):
        for user in self.user_list:
            if user.getName() == name:
                return user.getInterest()
        return -1

    def setUserInterest(self, name, interest):
        for user in self.user_list:
            if user.getName() == name:
                user.setInterest(interest)
                return 1
        return -1

    def getUserByName(self, name):
        for user in self.user_list:
            if user.getName() == name:
                return user
        return None

    def getUserList(self):
        name = []
        for u in self.user_list:
            print(u.getName())
            name.append(u.getName())
        return name


class SharedStorage:
    app = Flask(__name__)

    def __init__(cls):
        cls.app.run()

    @app.errorhandler(404)
    def not_found(error):
        return make_response(jsonify({'error': 'Not found'}), 404)

    @app.route('/addNewUser/<user_name>', methods=['POST'])
    def addNewUserAPI(user_name):
        UserManager().addNewUser(user_name)
        return jsonpickle.encode({'success':True}), 200

    @app.route('/checkBookAvailability/<book_title>', methods=['GET'])
    def checkBook(book_title):
        return jsonpickle.encode(Library().checkBook(book_title))

    @app.route('/getBooksFromCategory/<category>', methods=['GET'])
    def getBooksFromCategory(category):
        return jsonpickle.encode(Library().getBooksFromCategory(category))

    @app.route('/getUserInterest/<user_name>', methods=['GET'])
    def getUserInterest(user_name):
        interest = UserManager().getUserInterest(user_name)
        if interest == -1:
            abort(404)
        else:
            return jsonpickle.encode(interest)

    @app.route('/getUserByName/<user_name>', methods=['GET'])
    def getUserByName(user_name):
        user = UserManager().getUserByName(user_name)
        return jsonpickle.encode(user)

    @app.route('/setUserInterest/<user_name>/<interest>', methods=['POST'])
    def setUserInterest(user_name, interest):
        if UserManager().setUserInterest(user_name,interest) == 1:
            return jsonpickle.encode({'success':True}), 200
        else:
            abort(404)

    @app.route('/getRandomTitle', methods=['GET'])
    def getRandomTitle():
        return jsonpickle.encode(Library().getRandomTitle())



def main():
    api = SharedStorage()

if __name__ == '__main__':
    main()
