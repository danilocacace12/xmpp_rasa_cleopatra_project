#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod

class AbstractMUC(ABC):
    @abstractmethod
    def new_user(self, presence):
        pass

    @abstractmethod
    def muc_message(self, msg):
        pass

    @abstractmethod
    def muc_untagged_message(self, msg):
        pass

    @abstractmethod
    def muc_tagged_message(self, msg):
        pass

    @abstractmethod
    def private_message(self, msg):
        pass

    @abstractmethod
    def unactive_users(self):
        pass
