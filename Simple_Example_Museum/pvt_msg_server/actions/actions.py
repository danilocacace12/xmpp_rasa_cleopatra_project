# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import requests
import jsonpickle

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionarrangeShipment(Action):
    def name(self) -> Text:
        return "action_arrange_shipment"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        address = tracker.get_slot('address')
        province = tracker.get_slot('province')
        civic_number = tracker.get_slot('civic_number')

        r = requests.get('http://localhost:5000/getUserInterest/{}'.format(tracker.sender_id))
        book_name = jsonpickle.decode(r.content)

        answer = "Perfect, USER_NAME, the {} will be shipped to:\n{}, {}, {}".format(book_name, address, civic_number, province)

        r = requests.post('http://localhost:5000/setUserInterest/{}/{}'.format(tracker.sender_id, None))

        dispatcher.utter_message(text=answer)
        return []
